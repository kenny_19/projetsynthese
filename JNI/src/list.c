#include <stdlib.h>
#include <stdio.h>
#include "../include/list.h"



List * init(){
    List* l = (List *) calloc(1, sizeof(List));
    l->size = 0;
    l->head = NULL;
    l->tail = NULL;

    return l;
}

/*
 * créer et renvoie un nouveau nœud (LNode)
 */

static LNode *new_node(void *data) {   //Debug: Jette "*", corrige par "," dans 'calloc' , plus beaux

    //calloc gestion la memoire
    LNode *nouv = (LNode *) calloc(1, sizeof(LNode));
    nouv->data = data;
    nouv->next = NULL;
    nouv->prev = NULL;
    return nouv;

}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en tête de la liste *list
 */
void list_prepend(List *list, void *data) {



    //Créer un noeud
    LNode *noeud = new_node(data);
    noeud->next = list->head;
    //if (list->head != NULL)
    if (list->size != 0)
        list->head->prev = noeud;
    else
        list->tail = noeud;
    list->head = noeud;
    list->size++;


}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en queue de la liste *list
 */
void list_append(List *list, void *data) {

    //Créer un noeud
    LNode *noeud = new_node(data);
   
    if (list->size == 0)
        list->head = noeud;
    else{
        //maintenance de la liste
        list->tail->next = noeud;
        noeud->prev = list->tail;
    }

    list->tail = noeud;

    list->size++;
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément dans la liste *list après l'élément prev
 * ce dernier est supposé appartenir effectivement à la liste
 */
void list_insert_after(List *list, void *data, LNode *curr) {
    //LNode *nouv = new_node(data);
    /*
    if (curr == list->tail) {
        list_append(list, data);
    } else if (curr == list->head)
        list_prepend(list, data);
    else {
        nouv->next = curr->next;
        nouv->prev = curr;
        curr->next->prev = nouv;
        curr->next = nouv;
        list->size++;
        //Rien faire pour list->tail
    }
     */

    LNode *nouv = new_node(data);
    LNode *p = list->head;
    while (p != NULL) {
        if (p == curr) {
            if (p->next != NULL)
                p->next->prev = nouv;
            nouv->next = p->next;
            p->next = nouv;
            nouv->prev = p;
            break;
        }
        p = p->next;
    }


}

/*
 * supprimer le premier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_first(List *list) {    //参考 t.cn/EXVwtgb
    if (list->size != 0) {
        LNode *tete = list->head;
        LNode *nouvTete = tete->next;
        list->head = nouvTete;
        list->size--;
        free(tete);
        tete = NULL;
    }

}

/*
 * supprimer le dernier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_last(List *list) {
    if (list->size != 0) {
        LNode *n = list->tail;
        list->tail = n->prev;
        n->prev->next = NULL;
        n->prev = NULL;
        free(n);
        list->size--;
    }

}

/*
 * supprimer l'élément pointé par node de la liste *list
 * l'élément est supposé appartenir effectivement à la liste
 */
void list_remove_node(List *list, LNode *node) {
    //si node est tete
    if (node == list->head) {
        list_remove_first(list);
    }
        //si node en queue
    else if (node == list->tail) {
        list_remove_last(list);
    } else {
        LNode *p = list->head;
        while (p != NULL) {
            if (p == node) {
                node->prev->next = node->next;
                node->next->prev = node->prev;
                node->prev = NULL;
                node->next = NULL;
                list->size--;
                free(node);
                break;
            }
            p = p->next;
        }
    }
}

/*
 * permute les positions des nœuds curr et curr->next
 * dans la liste list
 */
void list_exchange_curr_next(List *list, LNode *curr) {
    LNode *p = list->head;
    if (list->size >= 2) {
        while (p != NULL) {
            if (p == curr) {
                LNode *p2 = p->next;
                LNode *p3 = NULL;
                LNode *pa = p->prev;
                if (p2 != NULL) {
                    p3 = p->next->next;
                }
                //Si le noeud se trouve en tête
                if (p2 == NULL) {   //P2 == NULL Cas 1 et 2
                    //Rien à faire
                } else if (p3 == NULL && p2 != NULL && pa == NULL) { //  P est head, et p2 est tail
                    list->head = p2;
                    list->tail = p;

                    p2->prev = NULL;
                    p2->next = p;
                    p->prev = p2;
                    p->next = NULL;
                } else if (pa == NULL && p2 != NULL && p3 != NULL) {  // Normale mais p est Head
                    list->head = p2;
                    //tail pas constaté
                    p2->prev = NULL;
                    p2->next = p;
                    p->prev = p2;
                    p2->next = p3;
                    p3->prev = p;
                } else if (pa != NULL && p2 != NULL && p3 == NULL) { // Normale mais p2 est tail
                    list->tail = p;

                    p->next = NULL;
                    p->prev = p2;
                    p2->next = p;
                    p2->prev = pa;
                    pa->next = p2;
                } else {    // Normal: Size>=4 && p->prev != NULL && p->NEXT != NULL && p->next->next != NULL
                    p->next = p3;
                    p->prev = p2;
                    p3->prev = p;
                    p2->next = p;
                    p2->prev = pa;
                    pa->next = p2;
                }


                break;
            }
            p = p->next;
        }
    }

}

/*
 * supprimer tous les éléments de la liste *list
 * sans pour autant supprimer leurs données (data)
 * qui sont des pointeurs
 */
void list_destroy(List *list) { //OK
    while (list->size > 0) {
        list_remove_first(list);
    }
    free(list);


}
