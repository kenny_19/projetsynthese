#include <stdio.h>
#include <stdlib.h>
#include "../include/geometry.h"
#include "../include/rational.h"


/*
 * affiche le point p
 */
void display_point(const Point p) {
    printf("(");
    display_rational(p.x);
    printf(",");
    display_rational(p.y);
    printf(")");
}

/*
 * affiche le segment s
 */
void display_segment(const Segment s) {
    printf("from: ");
    display_point(s.begin);
    printf(" to: ");
    display_point(s.end);
}

/*
 * renvoie 1 si le point d'intersection key1 précède le point
 * d'intersection key2, 0 sinon
 */

int point_prec(Point key1, Point key2) {
    if (lt(key1.x, key2.x))
        return 1;
    else if (eq(key1.x, key2.x) && gt(key1.y, key2.y))
        return 1;
    else
        return 0;
}

/*
 * renvoie 1 si s1 précède s2, 0 sinon
 */
int seg_prec(Segment s1, Segment s2, Rational x) {  //x=balayage


    Rational minx1 = min(s1.begin.x, s1.end.x); //s1.begin.x
    Rational maxx1 = max(s1.begin.x, s1.end.x); //s1.end.x
    Rational minx2 = min(s2.begin.x, s2.end.x); //s2.begin.x
    Rational maxx2 = max(s2.begin.x, s2.end.x); //s2.end.x

    // Rational miny1 = min(s1.begin.y, s1.end.y);
    // Rational maxy1 = max(s1.begin.y, s1.end.y);
    // Rational miny2 = min(s2.begin.y, s2.end.y);
    // Rational maxy2 = max(s2.begin.y, s2.end.y);

    if (gte(x, minx1) && lte(x, maxx1) && gte(x, minx2) &&
        lte(x, maxx2)) {   //s1 précède que s2 donc s1, s2 ont le point d'intersection avec x

        Rational x1 = s1.begin.x;
        Rational x2 = s1.end.x;
        Rational y1 = s1.begin.y;
        Rational y2 = s1.end.y;

        Rational x3 = s2.begin.x;
        Rational x4 = s2.end.x;
        Rational y3 = s2.begin.y;
        Rational y4 = s2.end.y;

        Rational yy1, yy2;  //Pour obtenir l'ordonnée de y1 et y2 (Point d'interation avec s1, s2)
        if (neq(x1, x2)) {
            //yy1 = ((x * (y2 - y1) - (x1 * y2))+ x2*y1) / (x2 - x1);
            // yy1 = (rdiv(radd(rsub(rmul(x, rsub(y2, y1)), rmul(x1, y2)), rmul(x2, y1)), rsub(x2, x1)));
            // yy2 = (rdiv(radd(rsub(rmul(x, rsub(y4, y3)), rmul(x3, y4)), rmul(x4, y3)), rsub(x4, x3)));

            yy1 = radd(rdiv(rmul(rsub(y2, y1), rsub(x, x1)), rsub(x2, x1)), y1);
            yy2 = radd(rdiv(rmul(rsub(y4, y3), rsub(x, x3)), rsub(x4, x3)), y3);
            if (gt(yy1, yy2))
                return 1;
            else
                return 0;

        } else
            return 0;

    } else {
        return 0;
    }









    /*
    //on renvoie 1 ,si on trouve le point avec le plus grand y dans s1
    //on renvoie 0 ,si on trouve pas
    //pq sont les deux point de s1
    //rs sont les deux point de s2

    //pour connaître la K1, k2 de deux segments(droit)
    Rational dpqx = rsub(s1.end.x, s1.begin.x), dpqy = rsub(s1.end.y, s1.begin.y);  //d:delta
    Rational drsx = rsub(s2.end.x, s2.begin.x), drsy = rsub(s2.end.y, s2.begin.y);
    //xb est droit balayage x
    //ypq=dpqy/dpqx*(xb-xq)
    //yrs=drsy/drsx*(xb-xr)
    Rational ypq = rmul(rdiv(dpqy, dpqx), rsub(x, s1.begin.x));
    Rational yrs = rmul(rdiv(drsy, drsx), rsub(x, s2.begin.x));
    return gt(ypq, yrs);*/
}

/*
 * renvoie 1 si s1 et s2 ont une intersection, 0 sinon
 */
int intersect(Segment s1, Segment s2) { //L'origine: Juste 3 vecteurs font les opérations

    Rational x1, x2, x3, y1, y2, y3;
    x1.num = 0;
    x1.den = 0;
    x2.num = 0;
    x2.den = 0;
    x3.num = 0;
    x3.den = 0;
    y1.num = 0;
    y1.den = 0;
    y2.num = 0;
    y2.den = 0;
    y3.num = 0;
    y3.den = 0;

    x1 = rsub(s1.end.x, s1.begin.x);
    y1 = rsub(s1.end.y, s1.begin.y);
    x2 = rsub(s2.end.x, s1.begin.x);
    y2 = rsub(s2.end.y, s1.begin.y);
    x3 = rsub(s2.begin.x, s1.begin.x);
    y3 = rsub(s2.begin.y, s1.begin.y);

    Rational a1, a2;

    a1 = rsub(rmul(x1, y2), rmul(y1, x2));
    a2 = rsub(rmul(x1, y3), rmul(y1, x3));

    Rational mul;//pour tester

    mul = rmul(a1, a2);

    if ((mul.den < 0 && mul.num > 0) || (mul.den > 0 && mul.num < 0)) {
        //les deux point begin et end de s2 sont au dessus de s1 et au dessous de s1
        x1 = rsub(s2.end.x, s2.begin.x);
        y1 = rsub(s2.end.y, s2.begin.y);
        x2 = rsub(s1.end.x, s2.begin.x);
        y2 = rsub(s1.end.y, s2.begin.y);
        x3 = rsub(s1.begin.x, s2.begin.x);
        y3 = rsub(s1.begin.y, s2.begin.y);

        a1 = rsub(rmul(x1, y2), rmul(y1, x2));
        a2 = rsub(rmul(x1, y3), rmul(y1, x3));

        mul = rmul(a1, a2);
        if ((mul.den < 0 && mul.num > 0) || (mul.den > 0 && mul.num < 0))
            //les deux point begin et end de s1 sont au dessus de s2 et au dessous de s2
            return 1;
        else
            return 0;
    } else
        return 0;
}

/*
 * calcule et renvoie le point d'intersection entre s1 et s2
 */
Point *getIntersectionPoint(Segment s1, Segment s2) {

    int a = 0;
    Rational x1, x2, x3, x4, y1, y2, y3, y4;
    x1.num = 0;
    x1.den = 0;
    x2.num = 0;
    x2.den = 0;
    x3.num = 0;
    x3.den = 0;
    x4.num = 0;
    x4.den = 0;
    y1.num = 0;
    y1.den = 0;
    y2.num = 0;
    y2.den = 0;
    y3.num = 0;
    y3.den = 0;
    y4.num = 0;
    y4.den = 0;
    Point *res =NULL;
    Point p={{0,0},{0,0}}; //Point Interaction

    res = &p;
    a = intersect(s1, s2); //1 si interacte

    //Donner des valeurs
    x1 = s1.begin.x;
    y1 = s1.begin.y;
    x2 = s1.end.x;
    y2 = s1.end.y;
    x3 = s2.begin.x;
    y3 = s2.begin.y;
    x4 = s2.end.x;
    y4 = s2.end.y;

    //Point d'interaction

    p.x = rdiv(rsub(rmul(rsub(x1, x2), rsub(rmul(x3, y4), rmul(x4, y3))),
                    rmul(rsub(x3, x4), rsub(rmul(x1, y2), rmul(x2, y1)))),
               rsub(rmul(rsub(x3, x4), rsub(y1, y2)), rmul(rsub(x1, x2), rsub(y3, y4))));
    //p.y = ((y1 - y2) * (x3 * y4 - x4 * y3) - (x1 * y2 - x2 * y1) * (y3 - y4)) / ((y1 - y2) * (x3 - x4) - (x1 - x2) * (y3 - y4));
    p.y = rdiv(rsub(rmul(rsub(y1, y2), rsub(rmul(x3, y4), rmul(x4, y3))),
                    rmul(rsub(rmul(x1, y2), rmul(x2, y1)), rsub(y3, y4))),
               rsub(rmul(rsub(y1, y2), rsub(x3, x4)), rmul(rsub(x1, x2), rsub(y3, y4))));

    return res;
}
