#include <stdlib.h>
#include <stdio.h>


#include "../include/list.h"
#include "../include/geometry.h"
#include "../include/tree.h"
#include "../include/algo.h"

//#include "../include/util.h"
#include "jnilib_TestJNI.h"

JNIEXPORT void JNICALL Java_jnilib_TestJNI_printStrings
  (JNIEnv *env, jobject obj, jstring ss1, jstring ss2){
    const char *infilename = (*env)->GetStringUTFChars(env, ss1 , NULL);
    const char *outfilename = (*env)->GetStringUTFChars(env, ss2 , NULL);
    //myprint(infilename);
    //myprint(outfilename);






    List *list_segments=init(), *list_intersections=init();

    LNode *n1 = NULL, *n2 = NULL;
    Segment *s1, *s2;
    //Initialise res
    Point arr[100];
    int i = 0;


    list_segments = load_segments(infilename);

    if (list_segments->size == 0) {
        printf("Infilename incorrect!");
        exit(0);
    }


    n1 = list_segments->head;

    while (n1 != list_segments->tail) {
        n2 = n1->next;
        while (n2 != NULL) {
            s1 = (Segment *) (n1->data);
            s2 = (Segment *) (n2->data);
            if (intersect(*s1, *s2) == 1) {
                arr[i] = *(getIntersectionPoint(*s1, *s2));
                list_append(list_intersections, &(arr[i]));
                i++;
            }
            n2 = n2->next;
        }
        n1 = n1->next;
    }
    save_intersections(outfilename, list_intersections);
    
}