#include <stdlib.h>
#include <stdio.h>
#include "tree.h"


static void preorder(EventNode *node) {
    if (node != NULL) {
        display_point(node->key);
        printf("\n");
        preorder(node->left);
        preorder(node->right);
    }
}

static void inorder(EventNode *node) {
    if (node != NULL) {
        inorder(node->left);
        display_point(node->key);
        printf("\n");
        inorder(node->right);
    }
}

static void postorder(EventNode *node) {
    if (node != NULL) {
        postorder(node->left);
        postorder(node->right);
        display_point(node->key);
        printf("\n");
    }
}

// order = 0 (preorder), 1 (inorder), 2 (postprder)
void display_tree_keys(const EventTree *tree, int order) {
    switch (order) {
        case 0:
            preorder(tree->root);
            break;
        case 1:
            inorder(tree->root);
            break;
        case 2:
            postorder(tree->root);
            break;
        default:
            printf("display_tree_keys: non valid order parameter\n");
            exit(1);
    }
}

/*
 * renvoie un nouveau EventNode d'attribut
 *  <key, type, s1, s2, NULL, NULL>
 */
EventNode *new_event(Point key, int type, Segment *s1, Segment *s2) {
    EventNode *eventNode = (EventNode *) calloc(1, sizeof(EventNode));
    eventNode->key = key;
    eventNode->type = type;
    if (eventNode->type == 0) {
        eventNode->s1 = s1;
        eventNode->s2 = s2;
    } else {
        eventNode->s1 = s1;
        eventNode->s2 = NULL;
    }

    return eventNode;
}

/*
 * recherche la place dans tree du nouvel événement event
 * et l'insère.
 * L'ABR est modifié et la modification peut porter sur sa racine.
 */
void insert_event(EventTree *tree, EventNode *event) {
    EventTree *treeCopy = (EventTree *) calloc(1, sizeof(EventTree));

    if (tree->root == NULL) {    //cas arret
        tree->root = event;


    } else if (point_prec(event->key, tree->root->key) == 1) {
        treeCopy->root = tree->root->left;
        insert_event(treeCopy, event);
        tree->root->left = treeCopy->root;

    } else {

        treeCopy->root = tree->root->right;
        insert_event(treeCopy, event);
        tree->root->right = treeCopy->root;
    }


    tree->size++;
}

/*
 * trouve le prochain événement, le supprime de l'arbre et le renvoie
 */
EventNode *get_next_event(EventTree *tree) {
    EventNode *eventNode = NULL;

    if (tree == NULL)
        return NULL;
    else if (tree->root->left == NULL) {    //deux cas: 1.Right est null 2. right n'est pas null
        eventNode = new_event(tree->root->key, tree->root->type, tree->root->s1, tree->root->s2);
        EventNode* pt = tree->root; //Pour la 2ième condition

        if (tree->root->right == NULL) {
            free(tree->root);
            tree->size--;
        } else {
            tree->root = tree->root->right;
            free(pt);
            tree->size--;
        }

        return eventNode;

    } else //if (tree->root->left != NULL) { //Deux cas possibles: 1. right == NULL 2. right != NULL
    {
        EventNode *node = tree->root;
        EventNode *nodePere = NULL;

        while (node->left != NULL) {
            nodePere = node;
            node = node->left;
        }

        if (node->right != NULL)
            nodePere->left = node->right;
        else
            nodePere->left = NULL;

        eventNode = new_event(node->key, node->type, node->s1, node->s2);
        free(node);
        tree->size--;
        return eventNode;
    }
}

/*
 * renvoie 1 si le clef key existe dans l'ABR tree, O sinon
 */
int event_exists(EventTree *tree, Point key) {

    if (tree->size != 0) {
        Point p = tree->root->key;

        if ((eq(p.x, key.x) == 1 && (eq(p.y, key.y)) == 1)) {
            return 1;
        } else if (point_prec(key, p) == 1) {
            EventTree *eventTreeCopy = tree;
            if (eventTreeCopy->root->left != NULL) {
                eventTreeCopy->root = eventTreeCopy->root->left;
                eventTreeCopy->size--;
                return event_exists(eventTreeCopy, key);
            } else
                return 0;
        } else {
            EventTree *eventTreeCopy = tree;
            if (eventTreeCopy->root->right != NULL) {
                eventTreeCopy->root = eventTreeCopy->root->right;
                eventTreeCopy->size--;
                return event_exists(eventTreeCopy, key);
            } else
                return 0;

        }
    } else
        return 0;


}


