#include <stdlib.h>
#include <stdio.h>
#include "../include/list.h"
#include "../include/geometry.h"
#include "../include/tree.h"

/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List *load_segments(char *infilename) {
    List *segments = (List *) calloc(1, sizeof(List));

    FILE *fptr;
    if ((fptr = fopen(infilename, "r")) == NULL) {
        printf("Error while opening file %s.", infilename);
        exit(1);
    }
    int size;
    fscanf(fptr, "%d", &size);

    for (int i = 0; i < size; i++) {
        long a1, b1, c1, d1, a2, b2, c2, d2;
        fscanf(fptr, "%ld/%ld,%ld/%ld", &a1, &b1, &c1, &d1);
        fscanf(fptr, "%ld/%ld,%ld/%ld", &a2, &b2, &c2, &d2);
        Segment *s = (Segment *) malloc(sizeof(Segment));
        Point p1 = {{a1, b1},
                    {c1, d1}};
        Point p2 = {{a2, b2},
                    {c2, d2}};
        if (point_prec(p1, p2)) {
            (*s).begin = p1;
            (*s).end = p2;
        } else {
            (*s).begin = p2;
            (*s).end = p1;
        }
        list_append(segments, s);
    }
    fclose(fptr);
    return segments;
}

/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char *outfilename, List *intersections) {
    FILE *fptr;
    if ((fptr = fopen(outfilename, "w")) == NULL) {
        printf("Error while opening file %s.\n", outfilename);
        exit(1);
    }

    fprintf(fptr, "%d\n", intersections->size);
    LNode *curr = intersections->head;
    for (int i = 0; i < intersections->size; i++) {
        fprintf(fptr, "%ld/%ld,%ld/%ld\n",
                ((Point *) curr->data)->x.num,
                ((Point *) curr->data)->x.den,
                ((Point *) curr->data)->y.num,
                ((Point *) curr->data)->y.den);
        curr = curr->next;
    }
}

/*
 * exécute l'algorithme glouton sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void allPairs(char *infilename, char *outfilename) {

    List *list_segments = init(), *list_intersections = init();

    LNode *n1 = NULL, *n2 = NULL;
    Segment *s1, *s2;
    //Initialise res
    Point arr[100];
    int i = 0;


    list_segments = load_segments(infilename);

    if (list_segments->size == 0) {
        printf("Infilename incorrect!");
        exit(0);
    }


    n1 = list_segments->head;

    while (n1 != list_segments->tail) {
        n2 = n1->next;
        while (n2 != NULL) {
            s1 = (Segment *) (n1->data);
            s2 = (Segment *) (n2->data);
            if (intersect(*s1, *s2) == 1) {
                arr[i] = *(getIntersectionPoint(*s1, *s2));
                list_append(list_intersections, &(arr[i]));
                i++;
            }
            n2 = n2->next;
        }
        n1 = n1->next;
    }
    save_intersections(outfilename, list_intersections); //Enregistre la liste des points de l'intersection
}

/*
 * exécute l'algorithme Bentley-Ottmmann sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void BentleyOttmmann(char *infilename, char *outfilename) {
    List *list_Segment = load_segments(infilename); //Construit la liste des segments


    LNode *curr = list_Segment->head;   //Node de la liste

    Segment *s1 = NULL, *s2 = NULL;
    Point p1, p2;

    EventTree *arbreSegment = (EventTree*)calloc(1, sizeof(EventTree));
    EventNode *e1 = NULL, *e2 = NULL;


    //Initialise l'arbre pour stocker les segments qui sont dans la init_Segments (Initialise l'arbre de prorité des événements A)
    while (curr != NULL) {
        s1 = (Segment *) curr->data;
        p1 = s1->begin;
        p2 = s1->end;



        e1 = new_event(p1, 1, s1, NULL);
        e2 = new_event(p2, 2, s1, NULL);

        insert_event(arbreSegment, e1); //Ajourter début de segment a l'arbre
        insert_event(arbreSegment, e2);//Ajourter fin de segment a l'arbre

        curr = curr->next;
    }

    //Initialise LSA: l-s actif
    List *lsa = (List *) calloc(1, sizeof(List));

    //Initialse LI: la liste des points d'intersection
    List *li = (List *) calloc(1, sizeof(List));

    //Algo principal
    EventNode *e3 = get_next_event(arbreSegment);

    Segment *sPrev = NULL, *sSuiv = NULL;   //Pour I1 <- S inter Prédécesseur(LSA, S)
    Point *i1 = NULL, *i2 = NULL;
    EventNode *pointIntersect = NULL;
    while (e3 != NULL && (arbreSegment->size >= 0)) {
        Segment s = *(e3->s1);  //s<-Seg(E)
        if (e3->type == 1) { //Début de seg

            //Insérer(LSA,s)
            if (lsa->head == NULL) {
                list_prepend(lsa, s1);
            } else {    //head !=NULL


                if (seg_prec(s, *(Segment *) (lsa->head->data), e3->key.x) == 1) {  //seg devant la tête de lsa
                    list_prepend(lsa, &s);
                    sSuiv = (Segment *) lsa->head->next->data;
                } else if (seg_prec(s, *(Segment *) (lsa->tail->data), e3->key.x) == 0) {   //seg auprès de la queue de lsa
                    list_append(lsa, &s);
                    sPrev = (Segment *) lsa->tail->prev->data;
                } else {    //Milieu
                    curr = lsa->head;
                    while (curr != lsa->tail) {
                        if ((seg_prec(s, *(Segment *) (curr->data), e3->key.x) == 0 &&
                             seg_prec(s, *(Segment *) (curr->next->data), e3->key.x) == 1)) {
                            list_insert_after(lsa, &s, curr);
                            sPrev = (Segment *) curr->data;
                            sSuiv = (Segment *) curr->next->next->data;
                            break;
                        }
                        curr = curr->next;  //Déplacer le suivant
                    }
                }




            }

            //I1 <- S inter Prédécesseur(LSA, S)
            if (sPrev!= NULL && intersect(*(sPrev), s) == 1)
                i1 = getIntersectionPoint(*sPrev, s);

            //I2 <- S inter Successeur(LSA, S)
            if (sSuiv !=NULL && intersect(*(sSuiv), s) == 1)
                i2 = getIntersectionPoint(*sSuiv, s);


            if (i1 != NULL) {
                pointIntersect = new_event(*i1, 0, sPrev, e3->s1);
                //Ajouter à la li
                list_append(li, &(pointIntersect->key));

                //Ajouter à la l'arbre
                insert_event(arbreSegment, pointIntersect);
            }

            if (i2 != NULL) {
                pointIntersect = new_event(*i2, 0, e3->s1, sSuiv);

                //Ajouter à la li
                list_append(li, i2);

                //Ajouter à la l'arbre
                insert_event(arbreSegment, pointIntersect);
            }


        } else if (e3->type == 0) {   //Milieu
            Segment s_2 = *(e3->s2);    //s: s1, s_2: s2 ici
            Segment strouve, strouveSuiv;
            Segment s4 = *(Segment *) lsa->head->data, s5 = *(Segment *) lsa->tail->data;

            if (seg_prec(s, s_2, e3->key.x) == 1) {
                strouve = s;
                strouveSuiv = s_2;
            } else {
                strouve = s_2;
                strouveSuiv = s;
            }


            if ((eq(s4.begin.x, strouve.begin.x) == 1 && eq(s4.begin.y, strouve.begin.y) == 1 &&
                 eq(s4.end.x, strouve.end.x) == 1 &&
                 eq(s4.end.y, strouve.end.y) == 1) ||
                (eq(s4.end.x, strouve.begin.x) == 1 && eq(s4.end.y, strouve.begin.y) == 1 &&
                 eq(s4.begin.x, strouve.end.x) == 1 &&
                 eq(s4.begin.y, strouve.end.y) == 1))  //Le seg s(On veut trouver) dans la position tete de lsa
            {
                curr = lsa->head;
                list_exchange_curr_next(lsa, curr);
                if (curr->next != NULL)
                    sSuiv = (Segment *) curr->next->data;

                if (intersect(strouve, *sSuiv) == 1) {
                    i1 = getIntersectionPoint(strouve, *sSuiv);
                    pointIntersect = new_event(*i1, 0, &strouve, sSuiv);

                    //Ajouter à la li
                    list_append(li, i1);
                    //Ajouter à la l'arbre
                    insert_event(arbreSegment, pointIntersect);
                }

            } else if ((eq(strouveSuiv.begin.x, s5.begin.x) == 1 && eq(strouveSuiv.begin.y, s5.begin.y) == 1 &&
                        eq(strouveSuiv.end.x, s5.end.x) == 1 &&
                        eq(strouveSuiv.end.y, s5.end.y) == 1) ||
                       (eq(strouveSuiv.end.x, s5.begin.x) == 1 && eq(strouveSuiv.end.y, s5.begin.y) == 1 &&
                        eq(strouveSuiv.begin.x, s5.end.x) == 1 &&
                        eq(strouveSuiv.begin.y, s5.end.y) == 1))//Le seg s(On veut trouver) dans la position tail de lsa
            {
                curr = lsa->tail->prev;
                list_exchange_curr_next(lsa, curr);

                if (curr->prev->prev != NULL)
                    sPrev = (Segment *) curr->prev->prev->data;

                if (intersect(strouveSuiv, *sPrev) == 1) {
                    i1 = getIntersectionPoint(strouveSuiv, *sPrev);
                    pointIntersect = new_event(*i1, 0, sPrev, &strouveSuiv);

                    //Ajouter à la li
                    list_append(li, i1);
                    //Ajouter à la l'arbre
                    insert_event(arbreSegment, pointIntersect);
                }

            } else   //Milieu
            {
                curr = lsa->head;
                do {

                    //curr.data == strouve
                    s5 = *(Segment *) (curr->data);

                    if ((eq(strouve.begin.x, s5.begin.x) == 1 && eq(strouve.begin.y, s5.begin.y) == 1 &&
                         eq(strouve.end.x, s5.end.x) == 1 &&
                         eq(strouve.end.y, s5.end.y) == 1) ||
                        (eq(strouve.end.x, s5.begin.x) == 1 && eq(strouve.end.y, s5.begin.y) == 1 &&
                         eq(strouve.begin.x, s5.end.x) == 1 &&
                         eq(strouve.begin.y, s5.end.y) == 1)) {
                        sPrev = (Segment *) curr->prev->data;
                        sSuiv = (Segment *) curr->next->data;

                        list_exchange_curr_next(lsa, curr);


                        if (intersect(strouveSuiv, *sPrev) == 1) {
                            i1 = getIntersectionPoint(strouveSuiv, *sPrev);
                            pointIntersect = new_event(*i1, 0, sPrev, &strouveSuiv);

                            //Ajouter à la li
                            list_append(li, i1);
                            //Ajouter à la l'arbre
                            insert_event(arbreSegment, pointIntersect);
                        }

                        if (intersect(strouve, *sSuiv) == 1) {
                            i2 = getIntersectionPoint(strouve, *sSuiv);
                            pointIntersect = new_event(*i2, 0, &strouve, sSuiv);

                            //Ajouter à la li
                            list_append(li, i2);
                            //Ajouter à la l'arbre
                            insert_event(arbreSegment, pointIntersect);
                        }
                    }


                    curr = curr->next;
                } while (curr != lsa->tail);
            }


        } else {  //Fin

            Segment s4 = *(Segment *) lsa->head->data, s5 = *(Segment *) lsa->tail->data;
            if ((eq(s4.begin.x, s.begin.x) == 1 && eq(s4.begin.y, s.begin.y) == 1 && eq(s4.end.x, s.end.x) == 1 &&
                 eq(s4.end.y, s.end.y) == 1) ||
                (eq(s4.end.x, s.begin.x) == 1 && eq(s4.end.y, s.begin.y) == 1 && eq(s4.begin.x, s.end.x) == 1 &&
                 eq(s4.begin.y, s.end.y) == 1)) //Le seg s(On veut trouver) dans la position tete de lsa
                list_remove_first(lsa);

            else if ((eq(s5.begin.x, s.begin.x) == 1 && eq(s5.begin.y, s.begin.y) == 1 && eq(s5.end.x, s.end.x) == 1 &&
                      eq(s5.end.y, s.end.y) == 1) ||
                     (eq(s5.end.x, s.begin.x) == 1 && eq(s5.end.y, s.begin.y) == 1 && eq(s5.begin.x, s.end.x) == 1 &&
                      eq(s5.begin.y, s.end.y) == 1))//Le seg s(On veut trouver) dans la position tail de lsa
                list_remove_last(lsa);

            else {  //Le seg s(On veut trouver) dans lsa( sauf que la tête et la tail)
                //2ème node à la 2ème dernière
                LNode *n1 = lsa->tail->prev;

                curr = lsa->head->next;
                do {
                    s4 = *(Segment *) curr->data;

                    if ((eq(s4.begin.x, s.begin.x) == 1 && eq(s4.begin.y, s.begin.y) == 1 &&
                         eq(s4.end.x, s.end.x) == 1 && eq(s4.end.y, s.end.y) == 1) ||
                        (eq(s4.end.x, s.begin.x) == 1 && eq(s4.end.y, s.begin.y) == 1 && eq(s4.begin.x, s.end.x) == 1 &&
                         eq(s4.begin.y, s.end.y) == 1)) {

                        sPrev = (Segment *) curr->prev->data;
                        sSuiv = (Segment *) curr->next->data;

                        list_remove_node(lsa, curr);

                        if (intersect(*sPrev, *sSuiv) == 1) {
                            i1 = getIntersectionPoint(*sPrev, *sSuiv);
                            pointIntersect = new_event(*i1, 0, sPrev, sSuiv);

                            //Ajouter à la li
                            list_append(li, i1);
                            //Ajouter à la l'arbre
                            insert_event(arbreSegment, pointIntersect);

                        }
                    }


                } while (curr != n1);

            }
        }






        e3=get_next_event(arbreSegment);
    }

    display_point(*(Point*)li->head->data);
    save_intersections(outfilename, li); //Enregistre la liste des points de l'intersection

}






