#include <stdio.h>
#include <stdlib.h>
#include "../include/rational.h"


/*
 * renvoie le PGCD des a et b
 */
static long gcd(long a, long b) {  //OK
    if (a==0 || b==0 ){
        return 1;
    }
    while (a != b) {
        if (a < b) {
            b = b - a;
        } else if (a > b) {
            a = a - b;
        }
    }
    return a;
}

/*
 * garantit que:
 * - le dénominateur n'est pas zéro,
 * - le numérateur et le dénominateur sont premiers entre eux,
 * - si le nombre rationnel est négatif, alors le numérateur est négatif
 *   et le dénominateur est positif.
 */


static void simplify(Rational *r) { //OK
    if ((*r).den == 0) {
        printf("Rational.simplify: division by zero: ");
        display_rational(*r);
        printf("\n");
        exit(1);
    }

    long d = gcd(labs((*r).num), labs((*r).den));
    (*r).num /= d;
    (*r).den /= d;

    if ((*r).den < 0) {
        (*r).num *= -1;
        (*r).den *= -1;
    }
}


/*
 * affiche le nombre rationnel r
 */
void display_rational(const Rational r) {   //OK
    printf("%ld/%ld", r.num, r.den);
}

/*
 * renvoie a+b
 */
Rational radd(Rational a, Rational b) { //OK
    //pdem est le pgcd des dem de a et b

    Rational res;
    res.den = a.den * b.den;
    res.num = b.den * a.num + a.den * b.num;
    simplify(&res);
    return res;
}

/*
 * renvoie a-b
 */
Rational rsub(Rational a, Rational b) { //OK
    Rational c;
    simplify(&a);
    simplify(&b);
    a.den = a.den * b.den;
    a.num = a.num * b.den;
    b.num = b.num * a.den;

    c.den = a.den;
    c.num = a.num - b.num;

    simplify(&c);
    return c;
}

/*
 * renvoie a*b
 */
Rational rmul(Rational a, Rational b) { //OK
    Rational res;
    res.den = a.den * b.den;
    res.num = a.num * b.num;
    simplify(&res);
    return res;
}

/*
 * renvoie a/b
 */
Rational rdiv(Rational a, Rational b) { //OK
    Rational c;

    simplify(&a);
    simplify(&b);

    c.den = b.num;
    c.num = b.den;

    c = rmul(a, c);

    return c;
}

/*
 * renvoie 1 si a > b,  sinon 0
 */
int gt(Rational a, Rational b) {    //OK
    long pdem = gcd(a.den, b.den);
    if (a.num * pdem > b.num * pdem)
        return 1;
    else
        return 0;
}

/*
 * renvoie 1 si a < b,  sinon 0
 */
int lt(Rational a, Rational b) {    //OK

    a.num = a.num * b.den;
    b.num = b.num * a.den;
    return a.num < b.num ? 1 : 0;
}

/*
 * renvoie 1 si a >= b,  sinon 0
 */
int gte(Rational a, Rational b) {   //OK

    a.num = a.num * b.den;
    b.num = b.num * a.den;
    if (a.num > b.num)
        return 1;
    else if (a.num == b.num)
        return 1;
    else
        return 0;
}

/*
 * renvoie 1 si a <= b,  sinon 0
 */
int lte(Rational a, Rational b) {   //OK
    a.num = a.num * b.den;
    b.num = b.num * a.den;
    if (a.num < b.num)
        return 1;
    else if (a.num == b.num)
        return 1;
    else
        return 0;
}

/*
 * renvoie 1 si a == b,  sinon 0
 */
int eq(Rational a, Rational b) { //OK


    if (a.num * b.den == b.num * a.den)
        return 1;
    else
        return 0;

}

/*
 * renvoie 1 si a != b,  sinon 0
 */
int neq(Rational a, Rational b) {   //OK
    a.num = a.num * b.den;
    b.num = b.num * a.den;
    return a.num != b.num ? 1 : 0;
}

/*
 * renvoie le max entre a et b
 */
Rational max(Rational a, Rational b) {  //OK
    if (gte(a, b) == 1) {
        simplify(&a);
        return a;
    } else {
        simplify(&b);
        return b;
    }

}

/*
 * renvoie le min entre a et b
 */
Rational min(Rational a, Rational b) {  //OK
    Rational c;

    a.num = a.num * b.den;
    b.num = b.num * a.den;
    if (a.num < b.num)
        c.num = a.num;
    else
        c.num = b.num;
    c.den = a.den * b.den;
    simplify(&c);
    return c;
}
