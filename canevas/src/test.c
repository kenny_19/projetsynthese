#include <stdio.h>
#include <stdlib.h>

#include "geometry.h"



int main() {

    Point p = {{1,1}, {1,1}};
    Point p2 = {{5,1}, {5,1}};
    Point p3 = {{1,1}, {2,1}};
    Point p4 = {{5,1},{3,1}};

    printf("P1=");
    display_point(p);
    printf("\n");
    printf("P2=");
    display_point(p2);

    printf("\n");
    printf("Segment s1=");
    Segment s = {p, p2};
    display_segment(s);
    printf("\n");
    printf("Segment s2=");
    Segment s2 = {p3, p4};
    display_segment(s2);
    printf("\n");

    //Fonction prec
    int b = point_prec(p,p2);
    printf("b=%d\n", b);

    //Seg prec
    /*
    Rational ra = {8,5};
    int c = seg_prec(s,s2,ra);
    printf("\nc=%d", c);
*/
    int inter = intersect(s, s2);
    printf("\nInteraction est : %d", inter);

    Point *n = NULL;
    n=getIntersectionPoint(s, s2);
    display_point(*n);

















    return EXIT_SUCCESS;
}
