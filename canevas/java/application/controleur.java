package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class controleur implements Initializable {
	ObservableList<String> list=FXCollections.observableArrayList();
	@FXML
	private ChoiceBox<String> series;
	@FXML
	private Button b1;
	@FXML
	private Button b2;
	@FXML
	private Pane plan;
	private void loadData() {
		list.removeAll(list);
		String a="g";
		String b="b";
		list.addAll(a,b);
		series.getItems().addAll(list);
	}
	@Override
	public void initialize(URL location, ResourceBundle resources)  {
		// TODO Auto-generated method stub
		loadData();
	}
	
	
	public void drawInter(double x,double y) {
		Circle c1=new Circle();
		c1.setCenterX((x+1)*50);
		c1.setCenterY((y+1)*50);
		c1.setStroke(Color.BLUE);
		c1.setStrokeWidth(3);
		c1.setRadius(3);
		plan.getChildren().add(c1);
	}
	
	public void getPoint() {
		String seg[]=new String[50];
		seg=readFile();
		int nombreSeg=Integer.valueOf(seg[0]).intValue();
		double PointSeg[]=new double[50];
		int	k=0;
		double x,y;
		for(int j=1;j<=nombreSeg;j++) {
			for(int i=0;i<seg[j].length();i++) {
				if(seg[j].substring(i,i+1).equals("-")) {//si un num negative
					PointSeg[k]=(-1)*Integer.valueOf(seg[j].substring(i+1,i+2)).doubleValue();
					i+=2;
					k++;
				}else if(estNum(seg[j].substring(i,i+1))==1) {
					PointSeg[k]=Integer.valueOf(seg[j].substring(i,i+1)).doubleValue();
					i++;
					k++;
				}else {}
			}
			k=0;
			x=PointSeg[0]/PointSeg[1];
			y=PointSeg[2]/PointSeg[3];
			drawInter( x, y);
		}
		
		
	}
	
	public void drawSeg(double beginx,double beginy,double endx,double endy) {
		Line l1 = new Line((beginx+1)*50, (beginy+1)*50,(endx+1)*50 , (endy+1)*50);
		
		plan.getChildren().add(l1);
		
	}
	
	public void getSeg() {//obtenir les valeur dans input.txt
		String seg[]=new String[50];
		seg=readFile();
		for(String p:seg)
			System.out.println(p);
		int nombreSeg=Integer.valueOf(seg[0]).intValue();
			System.out.println(nombreSeg);//obteint nb segment
		int	k=0;
		double beginx,beginy,endx,endy;
		double PointSeg[]=new double[50];
			for(int j=1;j<=nombreSeg;j++) {
				for(int i=0;i<seg[j].length();i++) {
					if(seg[j].substring(i,i+1).equals("-")) {//si un num negative
						PointSeg[k]=(-1)*Integer.valueOf(seg[j].substring(i+1,i+2)).doubleValue();
						i+=2;
						k++;
					}else if(estNum(seg[j].substring(i,i+1))==1) {
						PointSeg[k]=Integer.valueOf(seg[j].substring(i,i+1)).doubleValue();
						i++;
						k++;
					}else {}
				}
				k=0;
				beginx=PointSeg[0]/PointSeg[1];
				endx=PointSeg[2]/PointSeg[3];
				beginy=PointSeg[4]/PointSeg[5];
				endy=PointSeg[6]/PointSeg[7];
				System.out.println(""+beginx+endx+beginy+endy);
				
				drawSeg(beginx,endx,beginy,endy);
			}
			
		
				}
	
	public String[] readFile() {
        String pathname =chooseFile(); // 绝对路径或相对路径都可以，写入文件时演示相对路径,读取以上路径的input.txt文件
        //防止文件建立或读取失败，用catch捕捉错误并打印，也可以throw;
        //不关闭文件会导致资源的泄露，读写文件都同理
        //Java7的try-with-resources可以优雅关闭文件，异常时自动关闭文件；详细解读https://stackoverflow.com/a/12665271
        String array[]=new String[50];
        int i=0;
        try (FileReader reader = new FileReader(pathname);
             BufferedReader br = new BufferedReader(reader) // 建立一个对象，它把文件内容转成计算机能读懂的语言
        ) {
            String line;
            //网友推荐更加简洁的写法
            while ((line = br.readLine()) != null) {
                // 一次读入一行数据
               //charger les valeur de document dans line et enregistrer dans array              
        		array[i]=line;
                i++;
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
        
	}
	
	public String chooseFile() {
		Stage mainStage = null;
		FileChooser fileChooser = new FileChooser();//构建一个文件选择器实例
		fileChooser.setTitle("Choisir un fichier TXT");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Text Files", "*.txt"));	//文本选择器
		File selectedFile = fileChooser.showOpenDialog(mainStage);
		//在实例中选择“打开文件”模式在传入窗口中显示。可以看出他返回用户选择文件的一个实例
		
		
		String path = selectedFile.getPath();
		return path;
	}
	
	public int estNum(String a) {//si a un num renvoie 1 sinon renvoie 0
		if(a.equals("0")||a.equals("1")||a.equals("2")||a.equals("3")||a.equals("4")||a.equals("5")||a.equals("6")||a.equals("7")||a.equals("8")||a.equals("9"))
			return 1;
		else
			return 0;
	}

}
